1) Single responsibility principle: **GameSettings.cs;**

2) Open–closed principle: **GuessNumberGameManager.cs**  and **GameManager.cs** method  NewGame();

3) Liskov substitution principle: **GuessNumberGameManager.cs**  and **GameManager.cs;**

4) Interface segregation principle: **IGame.cs, IConsoleReader.cs, INumberGenerator.cs**

5) Dependency inversion principle: **GuessNumberGameManager.cs** 8 line and **GuessNumberGame.cs** 8 line.
