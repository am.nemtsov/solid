﻿namespace GuessNumber
{
    internal static class GameSettingsBuilder
    {
        internal static GameSettings Build()
         {
            IConsoleReader consoleReader = new ConsoleReader();
            Console.WriteLine("\nEnter attempts count:");
            var attemptCount = consoleReader.ReadIntegerNumber();

            Console.WriteLine("\nEnter minimum value for guess number:");
            var minValue = consoleReader.ReadIntegerNumber();

            Console.WriteLine("\nEnter maximum value for guess number:");
            var maxValue = consoleReader.ReadIntegerNumber();

            return new GameSettings
            {
                AttemptCount = attemptCount,
                MinValue = minValue,
                MaxValue = maxValue,
            };
        }
    }
}
