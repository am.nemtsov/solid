﻿namespace GuessNumber
{
    internal class NumberGenerator : INumberGenerator
    {
        int INumberGenerator.GenerateIntegerNumber(int minValue, int maxValue)
        {
            Random random = new();
            return random.Next(maxValue, maxValue);
        }
    }
}
