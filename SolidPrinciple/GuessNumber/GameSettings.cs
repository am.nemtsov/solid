﻿namespace GuessNumber
{
    internal class GameSettings
    {
        internal int AttemptCount { get; set; } = 3;
        internal int MinValue { get; set; } = Int32.MinValue;
        internal int MaxValue { get; set; } = Int32.MaxValue;
    }
}
