﻿namespace GuessNumber
{
    internal class GuessNumberGameManager : GameManager
    {
        private readonly IConsoleReader _consoleReader;
        private readonly int _newGameItem = 1;

        internal GuessNumberGameManager(IConsoleReader consoleReader)
        {
            _consoleReader = consoleReader ?? throw new ArgumentNullException(nameof(consoleReader));
        }

        internal override void NewGame()
        {
            Console.WriteLine($"\n      Welcome to the game \"Guess the number.\" \n Press key:\n{_newGameItem} - Start a new game. \n2 - Settings.");

            var useDefaultSettings = _consoleReader.ReadIntegerNumber() == _newGameItem;
            var gameSettings = useDefaultSettings ? new GameSettings() : GameSettingsBuilder.Build();

            IGame<GameSettings> game = new GuessNumberGame(new ConsoleReader(), new NumberGenerator());
            game.Play(gameSettings);
        }
    }
}
