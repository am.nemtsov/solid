﻿namespace GuessNumber
{
    interface IGame<in T>
    {
        void Play(T t);
    }
}
