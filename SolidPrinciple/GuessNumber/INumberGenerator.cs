﻿namespace GuessNumber
{
    interface INumberGenerator
    {
        int GenerateIntegerNumber(int minValue, int maxValue);
    }
}
