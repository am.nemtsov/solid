﻿namespace GuessNumber
{
    internal class ConsoleReader : IConsoleReader
    {
        int IConsoleReader.ReadIntegerNumber()
        {
            var consoleAnswer = Console.ReadKey();
            int result;
            while (!int.TryParse(consoleAnswer.KeyChar.ToString(), out result))
            {
                Console.WriteLine("\n Enter valid integer number!");
                consoleAnswer = Console.ReadKey();
            }
            return result;
        }
    }
}
