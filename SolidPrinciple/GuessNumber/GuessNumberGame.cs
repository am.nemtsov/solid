﻿namespace GuessNumber
{
    internal class GuessNumberGame: IGame<GameSettings>
    {
        private readonly IConsoleReader _consoleReader;
        private readonly INumberGenerator _numberGenerator;

        internal GuessNumberGame(IConsoleReader consoleReader, INumberGenerator numberGenerator)
        {
            _consoleReader = consoleReader ?? throw new ArgumentNullException(nameof(consoleReader));
            _numberGenerator = numberGenerator ?? throw new ArgumentNullException(nameof(numberGenerator));
        }

        void IGame<GameSettings>.Play(GameSettings gameSettings)
        {
            var currentAttemptCount = 0;
            var generatedNumber = _numberGenerator.GenerateIntegerNumber(gameSettings.MinValue, gameSettings.MaxValue);
 
            while (currentAttemptCount != gameSettings.AttemptCount)
            {
                Console.WriteLine($"Try guess number. Current attepmt is {currentAttemptCount} from {gameSettings.AttemptCount}. Your choice is:");
                var userNumber = _consoleReader.ReadIntegerNumber();
                currentAttemptCount++;
                if (generatedNumber == userNumber)
                {
                    Console.WriteLine("\n\nYou are win!");
                    return;
                }
                Console.WriteLine("\nYou answer is incorrect!");
            }

            Console.WriteLine("\n\nYou are lost!");
        }
    }
}
