﻿namespace GuessNumber
{
    internal interface IConsoleReader
    {
        internal int ReadIntegerNumber();
    }
}
